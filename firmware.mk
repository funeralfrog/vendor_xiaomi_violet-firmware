# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi/violet-firmware/BTFM.bin:install/firmware-update/BTFM.bin \
    vendor/xiaomi/violet-firmware/NON-HLOS.bin:install/firmware-update/NON-HLOS.bin \
    vendor/xiaomi/violet-firmware/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi/violet-firmware/aop.mbn:install/firmware-update/aop.mbn \
    vendor/xiaomi/violet-firmware/cmnlib.mbn:install/firmware-update/cmnlib.mbn \
    vendor/xiaomi/violet-firmware/cmnlib64.mbn:install/firmware-update/cmnlib64.mbn \
    vendor/xiaomi/violet-firmware/devcfg.mbn:install/firmware-update/devcfg.mbn \
    vendor/xiaomi/violet-firmware/dspso.bin:install/firmware-update/dspso.bin \
    vendor/xiaomi/violet-firmware/hyp.mbn:install/firmware-update/hyp.mbn \
    vendor/xiaomi/violet-firmware/imagefv.elf:install/firmware-update/imagefv.elf \
    vendor/xiaomi/violet-firmware/km4.mbn:install/firmware-update/km4.mbn \
    vendor/xiaomi/violet-firmware/qupv3fw.elf:install/firmware-update/qupv3fw.elf \
    vendor/xiaomi/violet-firmware/storsec.mbn:install/firmware-update/storsec.mbn \
    vendor/xiaomi/violet-firmware/tz.mbn:install/firmware-update/tz.mbn \
    vendor/xiaomi/violet-firmware/uefi_sec.mbn:install/firmware-update/uefi_sec.mbn \
    vendor/xiaomi/violet-firmware/xbl.elf:install/firmware-update/xbl.elf \
    vendor/xiaomi/violet-firmware/xbl_config.elf:install/firmware-update/xbl_config.elf \
    vendor/xiaomi/violet-firmware/splash.img:install/firmware-update/splash.img
